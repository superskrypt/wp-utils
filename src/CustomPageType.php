<?php 

namespace Superskrypt\WpUtils;

class CustomPageType {
    const QUERY_ARG_KEY = "ss_page_type";

    public function __construct($slug, $label) {
        $this->slug = $slug;
        $this->label = $label;
        $this->add_hooks();
    }

    private function add_hooks() {
        add_action('admin_menu', array($this, 'add_custom_page_menu_link'));
        // add_action('admin_init', array($this, 'superskrypt_modify_editor_view_on_custom_page'));

        add_action('save_post', array($this, 'save_post_meta_on_new_custom_page'));
        add_filter('display_post_states', array($this, 'display_custom_page_post_state'), 10, 2);
    }

    public function add_custom_page_menu_link() {
        $url = esc_url(add_query_arg(CustomPageType::QUERY_ARG_KEY, $this->slug, 'post-new.php?post_type=page'));
        add_submenu_page('edit.php?post_type=page', sprintf(__('Dodaj stronę %s', 'superskrypt'), $this->label), sprintf(__('Dodaj stronę %s', 'superskrypt'), $this->label), 'publish_pages', $url);
    }

    function display_custom_page_post_state($states, $post) {
        if ($this->is_custom_page($post->ID)) {
            $states[] = $this->label;
        }
        return $states;
    }

    public function save_post_meta_on_new_custom_page($postID) {
        if (isset($_GET[CustomPageType::QUERY_ARG_KEY]) && $_GET[CustomPageType::QUERY_ARG_KEY] == $this->slug) {
            update_post_meta($postID, CustomPageType::QUERY_ARG_KEY, $this->slug);
        }
    }

    private function is_custom_page($pageID = 0) {
        $page_type = CustomPageType::get_page_type($pageID);
        return ($page_type == $this->slug);
    }

    public static function get_page_type($pageID) {
        return get_post_meta($pageID, CustomPageType::QUERY_ARG_KEY, true);
    }
}
