<?php 

namespace Superskrypt\WpUtils;

class Route {
    private $regexp;
    private $query_args;
    private $template;
    private $request_uri;
    private $matches;

    public function __construct($regexp, $template, $query_args = null, $route_args = null, $template_directory = true) {
        $this->regexp = $regexp;
        $this->query_args = $query_args;
        $this->template = $template;
        $this->route_args = $route_args;
        $this->template_directory = $template_directory;
        add_filter('template_include', array($this, 'match_route'), 20);
    }

    public function match_route($template) {
        $regexp = trailingslashit($this->regexp);
        $match = preg_match("@^$regexp$@i", trailingslashit($_SERVER['REQUEST_URI']), $this->matches);

        if ($match) {
            $this->redirect_if_no_trailing_slash();
            if ($this->query_args) {
                $this->prepare_wp_query_for_custom_template();
            }
            if ($this->route_args) {
                $this->parsed_route_args = map_deep($this->route_args, array($this, 'parse_query_values'));
                add_filter('ss_route_args', function() {return $this->parsed_route_args;});
            }
            $template = $this->template_directory ? trailingslashit(get_template_directory()) . $this->template : $this->template;
            status_header(200);
        }
        return $template;
    }

    public function prepare_wp_query_for_custom_template() {
        global $wp_query, $wp;
        $query_args = map_deep($this->query_args, array($this, 'parse_query_values'));
        $wp_query = new \WP_Query($query_args);
        $wp->handle_404();
    }

    public function parse_query_values($value) {
        if (isset($this->matches[$value])) {
            return $this->matches[$value];
        } else {
            return $value;
        }
    }

    public function redirect_if_no_trailing_slash() {
        $uri_tokens = explode('?', $_SERVER['REQUEST_URI'], 2);

        if (sizeof($uri_tokens) > 0 && $uri_tokens[0] === rtrim($uri_tokens[0], '/')) {
            $uri_tokens[0] .= '/';
            wp_redirect(implode('?', $uri_tokens));
            exit();
        }
    }
}
