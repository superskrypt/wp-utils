<?php

namespace Superskrypt\WpUtils;

class Post_PageType_Condition extends \Carbon_Fields\Container\Condition\Condition {
    public static function init() {
        \Carbon_Fields\Carbon_Fields::extend( Post_PageType_Condition::class, function( $container ) {
            $condition = new Post_PageType_Condition();
            $condition->set_comparers( \Carbon_Fields\Carbon_Fields::resolve( 'generic', 'container_condition_comparer_collections' ) );
            return $condition;
        } );

        add_filter('carbon_fields_post_meta_container_static_condition_types', function($types) {
            $types[] = 'post_pagetype';
            return $types;
        });
    }

    public function is_fulfilled( $environment ) {
        $post_id = $environment['post_id'];

        if ($post_id) {
            $custom_page_type = CustomPageType::get_page_type($post_id);
        } else if (isset($_GET[CustomPageType::QUERY_ARG_KEY])) {
            $custom_page_type = $_GET[CustomPageType::QUERY_ARG_KEY];
        } else {
            error_log("Cannot find CustomPageType meta nor url slug");
            $custom_page_type = '';
        }

        return $this->compare(
            $custom_page_type,
            $this->get_comparison_operator(),
            $this->get_value()
        );
    }
}
